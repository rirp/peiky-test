# Capas de la aplicacion
# Capas
En general la aplicación utiliza MVVM, pero adicionando 2 capas:

1. View: Son los view controller donde contienen netamente lógica de presentación y captura los eventos del usuario
2. ViewModel: Está encargado de realizar la logica de negocio y comunicarle a la vista los datos consultados a través del manager.
3. Manager: Está encargado de consultar la data de los servicios o del core data según sea el caso. No utilize Afnetworking sino utilice una implementación propia de user default.
4. Configuration: Manejar las instancias de las capas.
5. Router: Está capa es la que se encarga de hacer los llamados del view controller para la navegación de las vistas. Existe un Route general que se encarga hacer una llamada rápida de los datos.
6. Modelo: POJOs de los servicios.

# Preguntas
1. En qué consiste el principio de responsabilidad única? Cuál es su propósito? 
Cada clase, modulo o interfaz debe tener una responsabilidad. Es decir, una única razón por la que cambiar. Con el proposito de diminuir tiempo a la hora encontrar errores y facilitar la lectura del código.

2. Qué características tiene, según su opinión, un “buen” código o código limpio?
- Una buena nemotécmia
- No debe contener código duplicado.
- Respetar los principios de SOLID.
