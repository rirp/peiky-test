//
//  ViewControllerExtension.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit


extension UIStoryboard  {
    static func getViewController<T>(storyId: String, to: T.Type) -> T where T: UIViewController {
        let storyBoard = UIStoryboard(name:storyId, bundle: nil)
        let className = String(describing: to)
        let viewControllerValue = storyBoard.instantiateViewController(withIdentifier: className)
        return viewControllerValue as! T
    }
}
