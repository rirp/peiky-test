//
//  JSONExtension.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import SwiftyJSON

extension JSON {
    func turnToObject<T>(_ type: T.Type) -> T? where T: Decodable {
        do {
            if let response = self.dictionaryObject {
                guard let allData = try? JSONSerialization.data(withJSONObject:response,
                                                                options: .prettyPrinted) else {
                    return nil
                }
                return try JSONDecoder().decode(type, from: allData)
            }
        } catch let error {
            print(">> Error Get/Parese as Data Custom =\(error.localizedDescription) !!")
        }
        return nil
    }
}

extension Encodable {
  func asDictionary() -> [AnyHashable: Any] {
    do {
        let data = try JSONEncoder().encode(self)
        return try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String : Any]
    } catch {
        return [:]
    }
  }
}
