//
//  AppDelegate.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UITabBar.appearance().unselectedItemTintColor = UIColor.gray
        
        window = UIWindow(frame: UIScreen.main.bounds);
        let rootViewController = MainConfiguration.setup()
        
        let navigationViewController = UINavigationController(rootViewController: rootViewController)
        let navigationBarAppearace = UINavigationBar.appearance()
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationBarAppearace.titleTextAttributes = textAttributes
        navigationBarAppearace.tintColor = .white
        navigationBarAppearace.barTintColor = .primaryColor
        window?.rootViewController = navigationViewController
        window?.makeKeyAndVisible()
        
        return true
    }
}

