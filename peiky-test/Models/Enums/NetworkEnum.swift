//
//  NetworkEnum.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

enum InformationType: String{
    case popular = "popular"
    case upcoming = "upcoming"
    case toprated = "toprated"
}
