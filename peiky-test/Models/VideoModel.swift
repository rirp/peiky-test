//
//  VideoModel.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 5/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

struct VideoResponse: Codable {
    let id: Int
    let results: [VideoModel]?
}

struct VideoModel: Codable {
    let id: String
    let key: String?
    let site: String?
}
