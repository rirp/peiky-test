//
//  MovieModel.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

struct MovieResponse: Codable {
    let page: Int
    let results: [MovieModel]
    let totalPages: Int
    let totalResults: Int
    
    enum CodingKeys: String, CodingKey {
        case page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}

struct MovieModel: Codable {
    let id: Int
    let originalName: String?
    let originalTitle: String?
    let overview: String?
    let popularity: Double?
    let posterPath: String?
    let releaseDate: String?
    let firstAirDate: String?
    let seasonNumber: Int?
    let video: Bool?
    var informationType: InformationType? = .popular
    
    enum CodingKeys: String, CodingKey {
        case id, overview
        case originalName = "original_name"
        case originalTitle = "original_title"
        case popularity
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case firstAirDate = "first_air_date"
        case seasonNumber = "number_of_seasons"
        case video
    }
}

class MoviesData {
    var sectionName: String = "Movies"
    var sectionData: [MovieModel] = []
}
