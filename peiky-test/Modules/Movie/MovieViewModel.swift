//
//  MovieViewModel.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import RxSwift

class MovieViewModel: BaseViewModel {
    var moviesBackup: [MovieModel]?
    var movies: PublishSubject<[MovieModel]> = PublishSubject()
    var searchWord: PublishSubject<String> = PublishSubject()
    private var manager: MovieManager? = nil
    
    init(_ manager: MovieManager?) {
        self.manager = manager
    }
    
    func getData(with informationType: InformationType) {
        manager?.informationType = informationType
        self.loading.onNext(true)
        manager?.getMovie(by: .movie) { (result) in
            self.interpretatingResponse(result)
        }
    }
    
    func getData(by query: String) {
        if query.isEmpty {
            movies.onNext([])
        } else {
            manager?.informationType = .search
            self.loading.onNext(true)
            self.manager?.getMovie(by: query) { (result) in
                self.interpretatingResponse(result)
            }
        }
    }
    
    func find(by word: String) {
        let result = moviesBackup?.filter({
            return $0.originalTitle?.lowercased().contains(word.lowercased()) ?? false
                || $0.originalName?.lowercased().contains(word.lowercased()) ?? false
        })
        self.movies.onNext(result ?? [])
    }
    
    private func interpretatingResponse(_ result: MovieManager.Result) {
        switch(result) {
        case .success(let movies):
            self.moviesBackup = movies
            self.movies.onNext(movies)
            self.loading.onNext(false)
            break;
        case .failure(let failure):
            self.movies.onNext(self.manager?.dataFromDataBase ?? [])
            self.loading.onNext(false)
            self.error.onNext(failure)
            break;
        }
    }
}
