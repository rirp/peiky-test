//
//  MovieRouter.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

class MovieRouter {
    var view: MovieViewController?
    
    init(view: MovieViewController?) {
        self.view = view
    }
    
    func navigateToDetail(_ movie: MovieModel) {
        view?.navigate(module: GeneralRouterRoute.detail(movie))
    }
}
