//
//  MovieManager.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias GetMovies = (MovieManager.Result) -> Void

enum CategoryType: String {
    case movie = "movie"
    case tv = "tv"
    case none = ""
}

class MovieManager {
    var information: [MovieModel] =  []
    var informationType: InformationType = .popular
    private let persistenceContext = PersistenceManager.shared
    private var type: CategoryType = .movie
    private var result: GetMovies?
    
    init() {
        deleteOnMemory()
    }
    
    enum Result {
        case success([MovieModel])
        case failure(PKError)
    }
    
    lazy var dataFromDataBase: [MovieModel] = {
        let movies = self.persistenceContext.fetch(Movie.self)
        
        if movies.isEmpty {
            return []
        }
        var informationDB = [MovieModel]()
        movies.forEach {
            let movie = MovieModel(id: Int($0.movieId), originalName: $0.originalTitle,
                       originalTitle: $0.originalTitle, overview: $0.overview ?? "",
                       popularity: $0.popularity, posterPath: $0.posterPath ?? "" ,
                       releaseDate: $0.releaseDate, firstAirDate: $0.firstAirDate,
                       seasonNumber: Int($0.seasonNumber ?? "0"), video: $0.video,
                       informationType: InformationType(rawValue: $0.type ?? InformationType.popular.rawValue))
            informationDB.append(movie)
        }
        return informationDB.filter({ return $0.informationType == self.informationType })
    }()
}

extension MovieManager {
    func getMovie(by query: String, completion: @escaping GetMovies) {
        type = .none
        self.result = completion

        if let newQuery = query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            let url = "search/multi?api_key=\(APIManager.apiKey)&query=\(newQuery)"
            APIManager.requestData(url: url) { (response) in
                switch response {
                case .success(let returnJson):
                    self.receiveData(returnJson)
                case .failure(let failure):
                    switch failure {
                    case .connectionError:
                        self.result?(Result.failure(.internetError(NSLocalizedString("message.network", comment: ""))))
                    case .authorizationError(let errorJson):
                        self.result?(Result.failure(.serverMessage(errorJson["message"].stringValue)))
                    default:
                        self.result?(Result.failure(.serverMessage(NSLocalizedString("message.unknow", comment: ""))))
                    }
                }
            }
        }
        
    }
}

extension MovieManager {
    func getMovie(by categoryType: CategoryType? = nil, completion: GetMovies? = nil) {
        if categoryType != nil {
            self.type = categoryType!
        }
        
        if completion != nil {
            self.result = completion
        }
        let url = "\(type.rawValue)/\(informationType.rawValue)?api_key=\(APIManager.apiKey)"
        APIManager.requestData(url: url) { (response) in
            switch response {
            case .success(let returnJson):
                self.receiveData(returnJson)
            case .failure(let failure):
                switch failure {
                case .connectionError:
                    self.result?(Result.failure(.internetError(NSLocalizedString("message.network", comment: ""))))
                case .authorizationError(let errorJson):
                    self.result?(Result.failure(.serverMessage(errorJson["message"].stringValue)))
                default:
                    self.result?(Result.failure(.serverMessage(NSLocalizedString("message.unknow", comment: ""))))
                }
            }
        }
    }
    
    private func receiveData(_ returnJson: JSON) {
        self.addData(returnJson)
        self.validateType()
    }
    
    private func addData(_ returnJson: JSON) {
        if let result = returnJson.turnToObject(MovieResponse.self)?.results {
            if !result.isEmpty {
                information = result.filter({ $0.overview != nil && $0.popularity != nil && $0.posterPath != nil })
            }
        } else {
            self.result?(Result.failure(.serverMessage(NSLocalizedString("message.unknow", comment: ""))))
        }
    }
    
    private func validateType() {
        if self.type == .movie {
            self.type = .tv
            if self.informationType != .upcoming {
                self.getMovie()
            } else {
                saveData()
                self.result?(Result.success(information))
            }
        } else {
            saveData()
            self.result?(Result.success(information))
        }
    }
}

extension MovieManager {
    private func saveData() {
        if informationType != .search {
            self.deleteByInformationType()
            self.information.forEach {
                if let context = self.persistenceContext.context {
                    let movie = Movie(context: context)
                    movie.movieId = Double($0.id)
                    movie.originalTitle = $0.originalName == nil ? $0.originalTitle : $0.originalName
                    movie.overview = $0.overview
                    movie.popularity = $0.popularity!
                    movie.seasonNumber = $0.seasonNumber == nil ? String(describing: $0.seasonNumber) : nil
                    movie.type = self.informationType.rawValue
                    self.persistenceContext.save()
                }
            }
        }
    }
    
    private func deleteByInformationType() {
        let values = persistenceContext.fetch(Movie.self)
        values.forEach {
            if $0.type == informationType.rawValue {
                persistenceContext.context?.delete($0)
            }
        }
    }
    
    private func deleteOnMemory() {
        if !information.isEmpty {
            for index in 0 ... information.count  {
                information.remove(at: index)
            }
        }
    }
}
