//
//  MovieConfiguration.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class MovieConfiguration {
    static func setup(_ informationType: InformationType, parameters: [String: Any]? = nil) -> UIViewController {
        let viewController = UIStoryboard.getViewController(storyId: "Main", to: MovieViewController.self)
        
        let manager = MovieManager()
        let viewModel = MovieViewModel(manager)
        let router = MovieRouter(view: viewController)
        
        viewController.informationType = informationType
        viewController.viewModel = viewModel
        viewController.router = router
        return viewController
    }
}
