//
//  MovieViewController.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MovieViewController: BaseViewController {
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    @IBOutlet weak var searchView: UISearchBar!

    var viewModel: MovieViewModel? = nil
    var informationType: InformationType = .popular
    var router: MovieRouter?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupBinds(with: viewModel!)
        moviesCollectionView!.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        moviesCollectionView.backgroundColor = .clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if informationType == .search {
            searchView.becomeFirstResponder()
        } else {
            viewModel?.getData(with: informationType)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        viewModel?.cancelRequest()
    }

    override func setupBinds<T : IBaseViewModel>(with viewModel: T) {
        super.setupBinds(with: viewModel)
        dataSource()
        animation()
        setupSearchView()
    }
}

extension MovieViewController {
    private func setupSearchView() {
        searchView.rx.text.orEmpty
            .throttle(.milliseconds(500), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .observeOn(MainScheduler.instance)
            .subscribe { (query) in
                if let query = query.element {
                    if self.informationType == .search {
                        self.viewModel?.getData(by: query)
                    } else {
                        if query.isEmpty {
                            self.viewModel?.movies.onNext(self.viewModel?.moviesBackup ?? [])
                        } else {
                            self.viewModel?.find(by: query)
                        }
                    }
                } else {
                    self.viewModel?.movies.onNext(self.viewModel?.moviesBackup ?? [])
                }
        }.disposed(by: disposeBag)
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneWithNumberPad))
        keyboardDoneButtonView.setItems([doneButton, UIBarButtonItem()], animated: true)
        searchView.inputAccessoryView = keyboardDoneButtonView
    }
    
    @objc func doneWithNumberPad() {
        searchView.resignFirstResponder()
    }
    
    private func dataSource() {
        // Data source
        let nibName = String(describing: MovieCollectionViewCell.self)
        moviesCollectionView.register(UINib(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: String(describing: MovieCollectionViewCell.self))

        viewModel?.movies.bind(to: moviesCollectionView.rx.items(cellIdentifier: nibName,
                                                                 cellType: MovieCollectionViewCell.self)) {  (row, movie,cell) in
                cell.data = movie
            }.disposed(by: disposeBag)
        
        moviesCollectionView.rx.modelSelected(MovieModel.self)
            .subscribe(onNext: { (model) in
                self.router?.navigateToDetail(model)
            }).disposed(by: disposeBag)
        
        moviesCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
    }
    
    private func animation() {
        // Animation
        moviesCollectionView.rx.willDisplayCell
            .subscribe(onNext: ({ (cell,indexPath) in
                cell.alpha = 0
                let transform = CATransform3DTranslate(CATransform3DIdentity, 0, -250, 0)
                cell.layer.transform = transform
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                    cell.alpha = 1
                    cell.layer.transform = CATransform3DIdentity
                }, completion: nil)
            }))
            .disposed(by: disposeBag)
    }
}

extension MovieViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let cellWidth = (width - 30) / 2 // compute your cell width
        return CGSize(width: cellWidth, height: cellWidth / 0.65)
    }
}
