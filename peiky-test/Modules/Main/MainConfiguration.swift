//
//  MainConfiguration.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class MainConfiguration {
    static func setup() -> UIViewController {
        let view = UIStoryboard.getViewController(storyId: "Main", to: ViewController.self)
        return view
    }
}
