//
//  ViewController.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let tabViewController = UITabBarController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTabs()
    }


    public func setupTabs() {
        tabViewController.tabBar.barStyle = .default
        tabViewController.tabBar.tintColor = .primaryColor
        tabViewController.tabBar.unselectedItemTintColor = .unselectedColor
        
        let search = getViewController(title: "Search", image: "magnifyingglass.circle", informationType: .search)
        let popular = getViewController(title: "Popular", image: "star", informationType: .popular)
        let upComing = getViewController(title: "Up coming", image: "bell", informationType: .upcoming)
        let topRating = getViewController(title: "Top rating", image: "list.number", informationType: .toprated)

        tabViewController.viewControllers = [popular, topRating, upComing, search].map { UINavigationController(rootViewController: $0)}
        self.view.addSubview(tabViewController.view)
    }
    
    private func getViewController(title: String, image: String, informationType: InformationType) -> UIViewController {
        let viewController = MovieConfiguration.setup(informationType)
        viewController.title = title
        let image = UIImage(systemName: image)?
            .withRenderingMode(.alwaysOriginal)
            .with(color: .unselectedColor)
        viewController.tabBarItem.image = image
        return viewController
    }
}

