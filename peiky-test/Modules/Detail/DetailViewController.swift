//
//  File.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import YoutubePlayer_in_WKWebView

class DetailViewController: BaseViewController {
    let insets: CGFloat = 15
    
    @IBOutlet weak var originalNameLabel: UILabel!
    @IBOutlet weak var dateTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var popularyLabel: UILabel!
    @IBOutlet weak var overviewLabel: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var webView: WKYTPlayerView!
    
    var movie: PublishSubject<MovieModel> = PublishSubject()
    var movieElement: MovieModel!
    var viewModel: DetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBinds(with: viewModel!)
        self.setupView()
        self.setData()
    }
    
    override func setupBinds<T : IBaseViewModel>(with viewModel: T) {
        super.setupBinds(with: viewModel)
        
        self.viewModel?.hasNotKey
            .bind(to: videoView.rx.isHidden)
            .disposed(by: disposeBag)
        
        self.viewModel?.videoKey
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { key in
                self.webView.load(withVideoId: key)
            }).disposed(by: disposeBag)
        
        self.viewModel?.movie.subscribe(onNext: { movie in
            self.viewModel?.getVideo()
        }).disposed(by: disposeBag)
        
        movie.bind(to: self.viewModel?.movie ?? movie)
            .disposed(by: disposeBag)
        
        movie.subscribe { (movie) in
            if let movie = movie.element {
                self.originalNameLabel.text = movie.originalName == nil ? movie.originalTitle : movie.originalName
                self.popularyLabel.text = "\(movie.popularity!)"
                self.overviewLabel.text = "\(movie.overview!)"
                self.imageView.loadImage(fromURL: "\(APIManager.baseImg)\(movie.posterPath!)")
                if let date = movie.releaseDate {
                    self.dateLabel.text = date
                } else if let date = movie.firstAirDate {
                    self.dateTitleLabel.text = "First air date"
                    self.dateLabel.text = date
                } else {
                    self.dateLabel.text = "----/--/--"
                }
            }
        }.disposed(by: disposeBag)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        viewModel?.cancelRequest()
    }
    
    private func setupView() {
    }
    
    private func setData() {
        movie.onNext(movieElement)
    }
}
