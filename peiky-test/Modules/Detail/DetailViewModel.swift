//
//  DetailViewModel.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import RxSwift

class DetailViewModel: BaseViewModel {
    let youtube = "YouTube"
    var movie: PublishSubject<MovieModel> = PublishSubject()
    var videoKey: PublishSubject<String> = PublishSubject()
    var hasNotKey: PublishSubject<Bool> = PublishSubject()
    var manager: DetailManager?
    
    init(_ manager: DetailManager?) {
        super.init()
        self.manager = manager
        
        self.getVideo()
    }
    
    func getVideo() {
        movie.subscribe { (movie) in
            let id = movie.element?.id
            self.loading.onNext(true)
            self.manager?.getVideo(with: String(describing: id!)) { (result) in
            switch(result) {
            case .success(let videos):
                self.setupVideo(videos)
            default:
                self.setupVideo(nil)
                }
            }
        }.disposed(by: disposable)
    }
    
    private func setupVideo(_ videos: [VideoModel]?) {
        self.loading.onNext(false)
        if !(videos?.isEmpty ?? true) {
            videos?.forEach({ video in
                if video.site == youtube && video.key != nil {
                    videoKey.onNext(video.key!)
                    hasNotKey.onNext(false)
                    return
                }
            })
        } else {
            hasNotKey.onNext(true)
        }
    }
}
