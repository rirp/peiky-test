//
//  DetailManager.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 5/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias GetVideos = (DetailManager.Result) -> Void

class DetailManager {
    enum Result {
        case success([VideoModel])
        case failure(String)
    }
    
    func getVideo(with id: String, completion: @escaping GetVideos) {
        let url = "movie/\(id)/videos?api_key=\(APIManager.apiKey)"
        APIManager.requestData(url: url) { (response) in
        switch response {
        case .success(let returnJson):
            completion(Result.success(self.getData(returnJson)))
        case .failure(let failure):
            switch failure {
            case .connectionError:
                completion(Result.failure(NSLocalizedString("message.network", comment: "")))
            case .authorizationError(let errorJson):
                completion(Result.failure(errorJson["message"].stringValue))
            default:
                completion(Result.failure(NSLocalizedString("message.unknow", comment: "")))
            }
            }
        }
    }
        
    private func getData(_ returnJson: JSON) -> [VideoModel] {
        if let result = returnJson.turnToObject(VideoResponse.self)?.results {
            return result
        }
        return [VideoModel]()
    }
}
