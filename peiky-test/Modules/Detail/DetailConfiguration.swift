//
//  DetailConfiguration.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import UIKit

class DetailConfiguraion {
    static func setup(_ movie: MovieModel) -> UIViewController {
        let viewController = UIStoryboard.getViewController(storyId: "Main", to: DetailViewController.self)
        
        let manager = DetailManager()
        let viewModel = DetailViewModel(manager)
        
        viewController.movieElement = movie
        viewController.viewModel = viewModel
        
        return viewController
    }
}
