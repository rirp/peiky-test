//
//  BaseViewModel.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import RxSwift

internal enum PKError {
    case internetError(String)
    case serverMessage(String)
}

protocol IBaseViewModel {
    var disposable: DisposeBag { get set }
    var loading: PublishSubject<Bool> { get set }
    var error: PublishSubject<PKError> { get set }
}

class BaseViewModel: IBaseViewModel {
    var disposable = DisposeBag()
    var loading: PublishSubject<Bool> = PublishSubject()
    var error : PublishSubject<PKError> = PublishSubject()
    
    public func cancelRequest() {
        APIManager.cancel()
    }
}
