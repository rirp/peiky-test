//
//  Movie+CoreDataProperties.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 6/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//
//

import Foundation
import CoreData


extension Movie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movie> {
        return NSFetchRequest<Movie>(entityName: "Movie")
    }

    @NSManaged public var firstAirDate: String?
    @NSManaged public var movieId: Double
    @NSManaged public var originalTitle: String?
    @NSManaged public var overview: String?
    @NSManaged public var popularity: Double
    @NSManaged public var posterPath: String?
    @NSManaged public var releaseDate: String?
    @NSManaged public var seasonNumber: String?
    @NSManaged public var type: String?
    @NSManaged public var video: Bool

}
