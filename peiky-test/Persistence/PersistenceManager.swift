//
//  PersistenceManager.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright © 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import CoreData

final class PersistenceManager {
    private init() {}
    static let shared = PersistenceManager()
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer? = {
        var container = NSPersistentContainer(name: "peiky_test")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                print("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var context = persistentContainer?.viewContext

    // MARK: - Core Data Saving support
    func save () {
        let context = self.persistentContainer?.viewContext
        if context?.hasChanges ?? false {
            do {
                try context?.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
            
       }
    }
    
    func fetch<T: NSManagedObject>(_ objectType: T.Type) -> [T] {
        let entityName = String(describing: objectType)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        do {
            let fetchedObject = try context?.fetch(fetchRequest) as? [T]
            return fetchedObject ?? [T]()
        } catch {
            return [T]()
        }
    }
}
