//
//  GeneralRouterRoute.swift
//  peiky-test
//
//  Created by Ronald Ivan Ruiz Poveda on 4/12/19.
//  Copyright (c) 2019 Ronald Ivan Ruiz Poveda. All rights reserved.
//  Modify By:  * Ari Munandar
//              * arimunandar.dev@gmail.com
//              * https://github.com/arimunandar

import Foundation
import UIKit

enum GeneralRouterRoute: IRouter {
    case main
    case movie
    case detail(MovieModel)
}

extension GeneralRouterRoute {
    var module: UIViewController? {
        switch self {
        case .movie:
            return MovieConfiguration.setup(.popular)
        case .detail(let movie):
            return DetailConfiguraion.setup(movie)
        default:
            return MainConfiguration.setup()
        }
    }
}
